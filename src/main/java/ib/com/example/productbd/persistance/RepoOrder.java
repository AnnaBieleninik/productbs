package ib.com.example.productbd.persistance;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Indexed;
import ib.com.example.productbd.POJOmodels.Order;

@Indexed
public interface RepoOrder extends CrudRepository<Order, Long>{
}
