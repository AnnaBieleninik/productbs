package ib.com.example.productbd.persistance;

import ib.com.example.productbd.POJOmodels.User;
import ib.com.example.productbd.POJOmodels.UserDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Indexed;

@Indexed
public interface RepoUser extends CrudRepository<UserDto, Long> {

}
