package ib.com.example.productbd.persistance;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Indexed;
import ib.com.example.productbd.POJOmodels.Customer;

@Indexed
public interface RepoCustomer extends CrudRepository<Customer, Long>{
}
