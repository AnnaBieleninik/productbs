package ib.com.example.productbd.persistance;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Indexed;
import ib.com.example.productbd.POJOmodels.Product;

@Indexed
public interface Repo extends CrudRepository<Product, Long> {

}
