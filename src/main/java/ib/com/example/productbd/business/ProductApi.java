package ib.com.example.productbd.business;

import ib.com.example.productbd.DBMock;
import ib.com.example.productbd.POJOmodels.Customer;
import ib.com.example.productbd.POJOmodels.Product;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Optional;

@Service
@RestController
@RequestMapping("/customer")
public class ProductApi {
    private DBMock dbMock;

    public ProductApi(DBMock dbMock) {
        this.dbMock = dbMock;
    }

    @GetMapping("product/all")
    public Iterable<Product> getAll(){
        return dbMock.getAllProduct();
    }

    @GetMapping("product")
    public Optional<Product> getById(@RequestParam Long index) {
        return dbMock.getProductById(index);
    }

    @PostMapping("admin/product")
    public Product add(@RequestBody Product product) {
        return dbMock.addProduct(product);
    }

    @PutMapping("admin/product")
    public Product update(@RequestParam Long index, @RequestBody Product newProduct) {
        return dbMock.getProductById(index)
                .map(product -> {
                    product.setName(newProduct.getName());
                    product.setPrice(newProduct.getPrice());
                    product.setAvailable(newProduct.isAvailable());
                    return dbMock.addProduct(product);
                })
                .orElseGet(() -> dbMock.addProduct(newProduct));
    }

    @PatchMapping("admin/order")
    public ResponseEntity<Product> modify(@RequestParam Long index, @RequestBody Map<String, Object> fields) {

        if (index <= 0 || fields == null || fields.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Product product = dbMock.getProductById(index)
                .orElseThrow(() -> new ResourceNotFoundException("User not found on :: "+ index));

        if( product == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        fields.forEach((k, v) -> {
            System.out.println("A "+ k + " "+ v);
            Field field = ReflectionUtils.findField(Product.class, k);
            assert field != null;
            field.setAccessible(true);
            ReflectionUtils.setField(field, product, v);
        });

        dbMock.addProduct(product);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }


    @DeleteMapping("admin/product")
    public ResponseEntity<Object> delete(@RequestParam Long index) {
        Product product = dbMock.getProductById(index)
                .orElseThrow(() -> new ResourceNotFoundException("User not found on :: "+ index));

        if( product == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        dbMock.deleteProduct(index);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }
}
