package ib.com.example.productbd.business;

import ib.com.example.productbd.DBMock;
import ib.com.example.productbd.POJOmodels.Customer;
import ib.com.example.productbd.POJOmodels.Order;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Optional;

@Service
@RestController()
@RequestMapping("/order")
public class OrderApi {
    private DBMock dbMock;

    public OrderApi(DBMock dbMock) {
        this.dbMock = dbMock;
    }

    @GetMapping("order/all")
    public Iterable<Order> getAll(){
        return dbMock.getAllOrder();
    }

    @GetMapping("order")
    public Optional<Order> getById(@RequestParam Long index) {
        return dbMock.getOrderById(index);
    }

    @PostMapping("admin/order")
    public Order add(@RequestBody Order order) {
        return dbMock.addOrder(order);
    }

    @PutMapping("admin/order")
    public Order update(@RequestParam Long index, @RequestBody Order newOrder) {
        return dbMock.getOrderById(index)
                .map(order-> {
                    order.setProduct(newOrder.getProduct());
                    order.setCustomer(newOrder.getCustomer());
                    order.setDate(newOrder.getDate());
                    order.setStatus(newOrder.getStatus());
                    return dbMock.addOrder(order);
                })
                .orElseGet(() -> dbMock.addOrder(newOrder));
    }

    @PatchMapping("admin/order")
    public ResponseEntity<Order> modify(@RequestParam Long index, @RequestBody Map<String, Object> fields) {

        if (index <= 0 || fields == null || fields.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Order order = dbMock.getOrderById(index)
                .orElseThrow(() -> new ResourceNotFoundException("User not found on :: "+ index));

        if( order == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        fields.forEach((k, v) -> {
            System.out.println("A "+ k + " "+ v);
            Field field = ReflectionUtils.findField(Order.class, k);
            assert field != null;
            field.setAccessible(true);
            ReflectionUtils.setField(field, order, v);
        });

        dbMock.addOrder(order);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }


    @DeleteMapping("admin/order")
    public ResponseEntity<Object> delete(@RequestParam Long index) {
        Order order = dbMock.getOrderById(index)
                .orElseThrow(() -> new ResourceNotFoundException("User not found on :: "+ index));

        if( order == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        dbMock.deleteOrder(index);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }
    }