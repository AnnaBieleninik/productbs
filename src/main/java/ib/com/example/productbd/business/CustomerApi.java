package ib.com.example.productbd.business;

import ib.com.example.productbd.POJOmodels.Customer;
import ib.com.example.productbd.DBMock;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Optional;

@Service
@RestController
@RequestMapping("/customer")
public class CustomerApi {

  private DBMock dbMock;

    public CustomerApi(DBMock dbMock) {
        this.dbMock = dbMock;
    }

    @GetMapping("customer/all")
    public Iterable<Customer> getAll(){
        return dbMock.getAllCostumers();
    }

    @GetMapping("customer")
    public Optional<Customer> getById(@RequestParam Long index) {
       return dbMock.getCostumerById(index);
    }

    @PostMapping("admin/customer")
    public Customer add(@RequestBody Customer customer) {
        return dbMock.addCostumer(customer);
    }

    @PutMapping("admin/customer")
    public Customer update(@RequestParam Long index, @RequestBody Customer newCustomer) {
        return dbMock.getCostumerById(index)
                .map(customer -> {
                    customer.setName(newCustomer.getName());
                    customer.setAddress(newCustomer.getAddress());
                    return dbMock.addCostumer(customer);
                })
                .orElseGet(() -> dbMock.addCostumer(newCustomer));
    }

    @PatchMapping("admin/customer")
    public ResponseEntity<Customer> modify(@RequestParam Long index, @RequestBody Map<String, Object> fields) {

        if (index <= 0 || fields == null || fields.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Customer customer = dbMock.getCostumerById(index)
                .orElseThrow(() -> new ResourceNotFoundException("User not found on :: "+ index));

        if( customer == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        fields.forEach((k, v) -> {
            System.out.println("A "+ k + " "+ v);
            Field field = ReflectionUtils.findField(Customer.class, k);
            assert field != null;
            field.setAccessible(true);
            ReflectionUtils.setField(field, customer, v);
        });

        dbMock.addCostumer(customer);
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @DeleteMapping("admin/customer")
    public ResponseEntity<Object> delete(@RequestParam Long index) {
        Customer customer = dbMock.getCostumerById(index)
                .orElseThrow(() -> new ResourceNotFoundException("User not found on :: "+ index));

        if( customer == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        dbMock.deleteCostumer(index);
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }
}
