package ib.com.example.productbd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
public class ApplicationSecurity extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Autowired
    public PasswordEncoderConfig passwordEncoder;


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("SELECT u.name, u.password_hash, u.id FROM user_dto u WHERE u.name=?")
                .authoritiesByUsernameQuery("SELECT u.name, u.role u WHERE u.name=?")
                .passwordEncoder(passwordEncoder.passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "**/order").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "**/product").hasRole("ADMIN")
                .antMatchers("**/admin/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "**/costumer").hasRole("USER")
                .antMatchers(HttpMethod.GET, "**/product").hasRole("USER")
                .antMatchers(HttpMethod.GET, "**/order").hasRole("USER")
                .antMatchers(HttpMethod.POST, "**/order").hasRole("USER")
                .antMatchers("/login").permitAll()
                .antMatchers("/console/**").permitAll()
                .and()
                .headers().frameOptions().disable()
                .and()
                .httpBasic();

    }

}
