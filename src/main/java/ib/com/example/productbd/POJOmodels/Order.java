package ib.com.example.productbd.POJOmodels;

import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Table(name = "orderstable")
@Entity
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @ManyToOne(fetch = FetchType.EAGER)
    private Customer customer;
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Product> product;
    private LocalDateTime date;
    private String status;

    public Order() {
    }

    public Order(Customer customer, Set<Product> product, LocalDateTime date, String status) {
        this.customer = customer;
        this.product = product;
        this.date = date;
        this.status = status;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setProduct(Set<Product> product) {
        this.product = product;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Set<Product> getProduct() {
        return product;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }
}
