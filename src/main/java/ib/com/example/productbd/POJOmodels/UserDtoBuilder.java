package ib.com.example.productbd.POJOmodels;

import ib.com.example.productbd.PasswordEncoderConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDtoBuilder {


    private PasswordEncoderConfig passwordEncoder;

    @Autowired
    public UserDtoBuilder(PasswordEncoderConfig passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto UserDto (User user){
       String name = user.getName();
       String role = user.getRole();
       String password = user.getPasswordHash();
       String passwordEncoded = passwordEncoder.passwordEncoder().encode(password);
        UserDto u = new UserDto(name, passwordEncoded, role);
        return u;
    }
}
