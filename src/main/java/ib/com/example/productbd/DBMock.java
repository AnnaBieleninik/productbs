package ib.com.example.productbd;

import ib.com.example.productbd.POJOmodels.*;
import ib.com.example.productbd.persistance.Repo;
import ib.com.example.productbd.persistance.RepoCustomer;
import ib.com.example.productbd.persistance.RepoOrder;
import ib.com.example.productbd.persistance.RepoUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class DBMock {
    private Repo repo;
    private RepoCustomer repoCus;
    private RepoOrder repoOrd;
    private RepoUser repoUs;

@Autowired
    public DBMock(Repo repo, RepoCustomer repoCus, RepoOrder repoOrd, RepoUser repoUs) {
        this.repo = repo;
        this.repoCus = repoCus;
        this.repoOrd = repoOrd;
        this.repoUs = repoUs;
    }

    public Iterable<Customer> getAllCostumers(){
        return repoCus.findAll();
    }

    public Optional<Customer> getCostumerById(Long index) {
       return repoCus.findById(index);
    }

    public Customer addCostumer(Customer customer) {
        return repoCus.save(customer);
    }

    public void deleteCostumer(Long index) {
        repoCus.deleteById(index);
    }

    public Iterable<Order> getAllOrder(){
        return repoOrd.findAll();
    }

    public Optional<Order> getOrderById(Long index) {
        return repoOrd.findById(index);
    }

    public Order addOrder(Order order) {
        return repoOrd.save(order);
    }

    public void deleteOrder(Long index) {
        repoOrd.deleteById(index);
    }

    public Iterable<Product> getAllProduct(){
        return repo.findAll();
    }

    public Optional<Product> getProductById(Long index) {
        return repo.findById(index);
    }

    public Product addProduct(Product product) {
        return repo.save(product);
    }

    public void deleteProduct(Long index) {
        repo.deleteById(index);
    }


    @EventListener(ApplicationReadyEvent.class)

    public void addCostumer(){
        User u1 = new User("X","1234","USER");
        User u2 = new User("Y","9876","ADMIN");

        UserDtoBuilder builder = new UserDtoBuilder(new PasswordEncoderConfig());
        UserDto user1 = builder.UserDto(u1);
        UserDto user2 = builder.UserDto(u2);

        repoUs.save(user1);
        repoUs.save(user2);

        System.out.println("I am in Start");
        Product p1 = new Product("Cereals", 3.99f, true);
        Product p2 = new Product("Bread", 4.59f, false);
        Product p3 = new Product("Eggs", 6.50f, true);
        Product p4 = new Product("Water", 2.79f, true);
        repo.save(p1);
        System.out.println("1st record added");
        repo.save(p2);
        System.out.println("2nd record added");
        repo.save(p3);
        System.out.println("3rd record added");
        repo.save(p4);
        System.out.println("3rd record added");
        Iterable<Product> all = repo.findAll();
        //System.out.println(all.getClassName());
        all.forEach(System.out::println);

        Customer c1 = new Customer("Alice", "Wiazow 30");
        Customer c2 = new Customer("Bob", "Jaworowa");
        repoCus.save(c1);
        System.out.println("1st customer added");
        repoCus.save(c2);
        System.out.println("2nd customer added");
        Iterable<Customer> all2 = repoCus.findAll();
        all2.forEach(System.out::println);

            Set<Product> products1 = new HashSet<>() {
                {
                    add(p1);
                    add(p3);
                }};
                Set<Product> products2 = new HashSet<>() {
                    {{
                        add(p4);
                        add(p3);
                    }}
            };

        Order o1 = new Order(c1, products1, LocalDateTime.of(
                LocalDate.of(2022, 04, 04),
                LocalTime.of(12,45,55)),"done");
        Order o2 = new Order(c2, products2, LocalDateTime.of(
                LocalDate.of(2022, 04, 02),
                LocalTime.of(21,54,00)),"done");

        repoOrd.save(o1);
        System.out.println("1st order added");
        repoOrd.save(o2);
        System.out.println("2nd order added");
        Iterable<Order> all3 = repoOrd.findAll();
        all3.forEach(System.out::println);

    }

}
