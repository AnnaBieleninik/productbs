package ib.com.example.productbd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class ProductBdApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductBdApplication.class, args);
    }
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/order")
                        .allowedHeaders("Origin, X-Requested-With, Content-Type, Accept")
                        .allowedMethods("PUT, POST, GET, DELETE, OPTIONS");
            }
        };
    }

}
